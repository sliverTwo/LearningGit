Git is a distributed version control system.
Git is free software distributed under the GPL.
git command
    git add <file> # add a file to commit
    git commit -m <"note"> # commit all file to git as a version,-m add a note to this version
    git status # show the workspace status 
    git diff # compare the worspace between workspace of git.
    git log # view the log
    git log --pretty=oneline # show the log with a different view
    git reset  --hard HEAD^ # 回退至上个版本
    git reflog # 显示所有版本号
    git reset --hard <指定版本号> # 回退至指定版本
    git diff HEAD --<fileName> # 查看git中版本与工作区版本的不同
    git checkout -- <fileName> # 丢弃工作区的修改
    git reset HEAD <fileName> # 丢弃工作区的修改(已经add到暂缓区),再执行上句
    git rm <fileName> #将删除的文件提交到暂缓区
    ssh-keygen -t rsa -C "youremail@example.com" # 产生rsa密钥对（默认保存在user/.ssh目录下）

    git remote add <远程库名> git@github.com:<username>/<仓库名>.git # 关联github上的仓库 远程库名自定义，可默认为origin
    git remote add <远程库名> git@gitee.com:<username>/<仓库名>.git # 关联码云上的仓库
    git remote rm <远程库名> # 解除已有的远程库关联
    git push -u <远程库名> <分支名> # 第一次推送至远程仓库
    git push <远程库名> <分支名> # 推送至远程仓库
    git clone git@github.com:<用户名>/<仓库名>.git # 从github上clone仓库到本地
    git checkout -b <分支名> # 创建一个分支并切换到这个分支
    git branch <分支名> # 创建一个分支
    git checkout <分支名> # 切换到指定分支
    git branch # 查看所有分支
    git branch # 查看所有分支 -d <分支名> 删除分支 -D <分支名> 强行删除分支 
    git merge <分支名> 将分支合并到master上 --no-ff 禁用Fast forward -m 添加注释
    git log --graph --pretty=oneline --abbrev-commit  # 查看分支合并情况 --graph查看分支合并图 --abbrev-commit显示简短版本号
    git stash # 修复bug时保存工作现场
    git stash pop # bug修复完成后恢复现场
    git remote # 查询远程仓库信息 -v 更加详细的信息
    # 多人协作流程
    首先，可以试图用git push <远程库名> branch-name推送自己的修改；
    如果推送失败，则因为远程分支比你的本地更新，需要先用git pull试图合并；
    如果合并有冲突，则解决冲突，并在本地提交；
    没有冲突或者解决掉冲突后，再用git push <远程库名> branch-name推送就能成功！
    如果git pull提示“no tracking information”，则说明本地分支和远程分支的链接关系没有创建，用命令git branch --set-upstream branch-name <远程库名>/branch-name
    # 打标签
    git tag <标签名> # 给当前版本添加一个标签
    git tag <标签名> <版本号> # 给指定版本添加一个标签
    git tag # 查看所有标签 添加便签并注释(-a <标签名>  -m <注释> ) -s 使用PGP签名 -d <标签名> 删除标签
    git show <标签名> # 查询标签信息
    git push <远程库名> <tagname> # 推送标签
    git push <远程库名> --tags # 推送所有标签
    git push <远程库名> :refs/tags/<tagname> # 删除远程标签(需现在本地删除)
